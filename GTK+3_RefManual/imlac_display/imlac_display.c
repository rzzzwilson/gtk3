#include <gtk/gtk.h>

#define SIZE_X  512
#define SIZE_Y  512

/* Surface to store current display drawings */
static cairo_surface_t *surface = NULL;

// display width and height
static int display_width;
static int display_height;

// last draw position
gdouble last_x = -1;
gdouble last_y = -1;

/********************************************************
 * Clear display to black.
 ********************************************************/

static void
clear_surface(void)
{
    cairo_t *cr = cairo_create(surface);

    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_paint(cr);
    cairo_destroy(cr);
    last_x = -1;
    last_y = -1;
}

/********************************************************
 * Create a new surface of the appropriate size to store the drawings
 ********************************************************/

static gboolean
configure_event_cb(GtkWidget *widget, GdkEventConfigure *event, gpointer data)
{
  if (surface)
      cairo_surface_destroy(surface);

  surface = gdk_window_create_similar_surface(gtk_widget_get_window(widget),
                                              CAIRO_CONTENT_COLOR,
                                              gtk_widget_get_allocated_width(widget),
                                              gtk_widget_get_allocated_height(widget));

  /* Initialize the surface to the "empty" colour */
  clear_surface();

  /* We've handled the configure event, no need for further processing. */
  return TRUE;
}

/********************************************************
 * Redraw the screen from the surface. Note that the ::draw
 * signal receives a ready-to-be-used cairo_t that is already
 * clipped to only draw the exposed areas of the widget
 ********************************************************/

static gboolean
draw_cb(GtkWidget *widget, cairo_t *cr, gpointer data)
{
    cairo_set_source_surface(cr, surface, 0, 0);
    cairo_paint(cr);
    return FALSE;
}

/********************************************************
 * Draw a line on the surface from given posn to given posn.
 ********************************************************/

static void
draw_line(GtkWidget *widget, gdouble from_x, gdouble from_y, gdouble to_x, gdouble to_y)
{
    cairo_t *cr = cairo_create(surface);  // get drawing surface

    if ((from_x >= 0) && (from_x < display_width) & (from_y >= 0) && (from_y < display_height))
    {
        cairo_set_source_rgb(cr, 1, 1, 1);
        cairo_set_line_width(cr, 1);
        cairo_move_to(cr, from_x, from_y);
        if (to_x >= 0)
        {   // only draw if on-screen
            cairo_line_to(cr, to_x, to_y);
    
            // invalidate the actual rectangle updated
            gdouble left = from_x;
            gdouble top = from_y;
            gdouble right = to_x;
            gdouble bottom = to_y;
    
            if (left > right)
            {
                gdouble tmp = right;
                right = left;
                left = tmp;
            }
            if (top > bottom)
            {
                gdouble tmp = bottom;
                bottom = top;
                top = tmp;
            }

            gtk_widget_queue_draw_area(widget, left, top, right, bottom);
        }
    }

    cairo_stroke(cr);
    cairo_destroy(cr);

    /* Now invalidate the affected region of the drawing area. */
//    gtk_widget_queue_draw_area(widget, 0, 0, display_width-1, display_height-1);
}

/********************************************************
 * Draw a line on the surface to the given position
 ********************************************************/

static void
draw_brush(GtkWidget *widget, gdouble x, gdouble y)
{
    cairo_t *cr = cairo_create(surface);  // get drawing surface

    if ((x >= 0) && (x < display_width) & (y >= 0) && (y < display_height))
    {
        cairo_set_source_rgb(cr, 1, 1, 1);
        cairo_set_line_width(cr, 1);
        cairo_move_to(cr, last_x, last_y);
        if (last_x >= 0)
        {   // only draw if on-screen
            cairo_line_to(cr, x, y);
    
            // invalidate the actual rectangle updated
            gdouble left = x;
            gdouble top = y;
            gdouble right = last_x;
            gdouble bottom = last_y;
    
            if (left > right)
            {
                gdouble tmp = right;
                right = left;
                left = tmp;
            }
            if (top > bottom)
            {
                gdouble tmp = bottom;
                bottom = top;
                top = tmp;
            }

            gtk_widget_queue_draw_area(widget, left, top, right, bottom);
        }
    }
    last_x = x;
    last_y = y;

    cairo_stroke(cr);
    cairo_destroy(cr);

    /* Now invalidate the affected region of the drawing area. */
//    gtk_widget_queue_draw_area(widget, 0, 0, display_width-1, display_height-1);
}

/********************************************************
 * Handle button press events by either drawing a rectangle
 * or clearing the surface, depending on which button was pressed.
 * The ::button-press signal handler receives a GdkEventButton
 * struct which contains this information.
 ********************************************************/

static gboolean
button_press_event_cb(GtkWidget *widget, GdkEventButton *event, gpointer data)
{
    /* paranoia check, in case we haven't gotten a configure event */
    if (surface == NULL)
        return FALSE;
  
    if (event->button == GDK_BUTTON_PRIMARY)
    {
        draw_brush(widget, event->x, event->y);
    }
    else if (event->button == GDK_BUTTON_SECONDARY)
    {
        cairo_surface_write_to_png(surface, "imlac_display.png");
        printf("wrote PNG file\n");
        clear_surface();
        gtk_widget_queue_draw(widget);
    }
  
    /* We've handled the event, stop processing */
    return TRUE;
}

/********************************************************
 * Handle motion events by continuing to draw if button 1 is
 * still held down. The ::motion-notify signal handler receives
 * a GdkEventMotion struct which contains this information.
 ********************************************************/

static gboolean
motion_notify_event_cb(GtkWidget *widget, GdkEventMotion *event, gpointer data)
{
    /* paranoia check, in case we haven't gotten a configure event */
    if (surface == NULL)
        return FALSE;

    if (event->state & GDK_BUTTON1_MASK)
        draw_brush(widget, event->x, event->y);
    last_x = event->x;
    last_y = event->y;

    /* We've handled it, stop processing */
    return TRUE;
}

/********************************************************
 * Close the main window.
 ********************************************************/

static void
close_window(void)
{
    if (surface)
        cairo_surface_destroy(surface);
}

/********************************************************
 * Draw a rectangle on the surface at the given position
 ********************************************************/

static void
activate(GtkApplication *app, gpointer user_data)
{
    GtkWidget *window;
    GtkWidget *frame;
    GtkWidget *drawing_area;
  
    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Drawing Area");
  
    g_signal_connect(window, "destroy", G_CALLBACK(close_window), NULL);
  
    gtk_container_set_border_width(GTK_CONTAINER(window), 1);
  
    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    gtk_container_add(GTK_CONTAINER(window), frame);
  
    drawing_area = gtk_drawing_area_new();
    /* set a minimum size */
    gtk_widget_set_size_request(drawing_area, SIZE_X, SIZE_Y);
  
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);
  
    /* Signals used to handle the backing surface */
    g_signal_connect(drawing_area, "draw", G_CALLBACK(draw_cb), NULL);
    g_signal_connect(drawing_area,"configure-event", G_CALLBACK(configure_event_cb), NULL);
  
    /* Event signals */
    g_signal_connect(drawing_area, "motion-notify-event", G_CALLBACK(motion_notify_event_cb), NULL);
    g_signal_connect(drawing_area, "button-press-event", G_CALLBACK(button_press_event_cb), NULL);
  
    /* Ask to receive events the drawing area doesn't normally
     * subscribe to. In particular, we need to ask for the
     * button press and motion notify events that want to handle.
     */
    gtk_widget_set_events(drawing_area, gtk_widget_get_events(drawing_area)
                                        | GDK_BUTTON_PRESS_MASK
                                        | GDK_POINTER_MOTION_MASK);
  
    gtk_widget_show_all(window);

    GtkAllocation* alloc = g_new(GtkAllocation, 1);
    gtk_widget_get_allocation(drawing_area, alloc);
    display_width = alloc->width;
    display_height = alloc->height;
    printf("display size is %dx%d\n", display_width, display_height);
    g_free(alloc);

    // draw some lines
    for (int x = 0; x < SIZE_X; x += 64)
    {
        draw_line(drawing_area, x, 0, x, SIZE_Y-1);
    }
    for (int y = 0; y < SIZE_Y; y += 64)
    {
        draw_line(drawing_area, 0, y, SIZE_X-1, y);
    }
}

/********************************************************
 * The main() start.
 ********************************************************/

int
display_init(int argc, char **argv)
{
    GtkApplication *app;
    int status;
  
    app = gtk_application_new("com.gmail.rzzzwilson", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
  
    return status;
}

int
main(int argc, char **argv)
{
    return display_init(argc, argv);
}
