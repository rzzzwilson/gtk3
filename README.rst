Code used to learn GTK3.

Tutorials followed are:

GTK+3_RefManual:

    For GTK+3.
    https://developer.gnome.org/gtk3/stable/gtk-getting-started.html

tutorial:

    This is a GTK2 tutorial!
    https://python-gtk-3-tutorial.readthedocs.io/en/latest/
